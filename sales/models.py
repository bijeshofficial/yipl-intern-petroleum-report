from django.db import models

# Create your models here.


class Sale(models.Model):
    year = models.CharField(max_length=4)
    petroleum_product = models.CharField(max_length=50)
    sale = models.IntegerField()

    def __str__(self):
        return self.petroleum_product + ' | ' + str(self.sale) + ' | ' + self.year
