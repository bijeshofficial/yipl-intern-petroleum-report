from django.urls import include, path
from .views import index, generating_report, SaleView

from rest_framework import routers

router = routers.DefaultRouter()
router.register('sale/api', SaleView)

urlpatterns = [
    path('', index, name='home'),
    path('report/', generating_report, name='report'),
    path('', include(router.urls)),
]
