from rest_framework import serializers
from .models import Sale


class saleSerializers(serializers.ModelSerializer):
    '''
        Tried using rest framework to post data to database.
    '''
    class Meta:
        model = Sale
        fields = '__all__'
