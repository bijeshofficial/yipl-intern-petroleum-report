from django.shortcuts import render
import requests
import json
from django.http import JsonResponse
from .models import Sale
from django.db.models import Avg, Max, Min
from .serializers import saleSerializers
from rest_framework import viewsets, mixins
from rest_framework.generics import GenericAPIView

# Create your views here.


class SaleView(viewsets.ModelViewSet):
    queryset = Sale.objects.all()
    serializer_class = saleSerializers


# class CreateSaleView(mixins.CreateModelMixin, GenericAPIView):
#     def post(self, request, *args, **kwargs):
#         return self.create(request, *args, **kwargs)


def index(request):
    '''
        index function where i am loading data to database withour using rest framework
        used trivial method
    '''

    url = 'https://raw.githubusercontent.com/younginnovations/internship-challenges/master/programming/petroleum-report/data.json'
    # getting the data from the url

    response_data = requests.get(url=url)

    # converting the response result into json data
    json_data = response_data.json()

    # dumping the data as it takes a json object and returns a string
    dumping_json = json.dumps(json_data)

    '''taking the dumped string and changing it back to json 
    because the original json_data is a list'''
    actual_json = json.loads(dumping_json)
    # looping throught the data in order to store it in the database
    # First checking if the database is empty else the data gets stored every time page refreshes
    if not Sale.objects.all().first():
        for i in range(len(actual_json)):
            data = Sale(
                year=actual_json[i]['year'], petroleum_product=actual_json[i]['petroleum_product'], sale=actual_json[i]['sale'])
            data.save()

    # Getting the data from data and passing it at contect to show in the home page
    data_for_report = Sale.objects.all()
    context = {
        'data': data_for_report,
    }

    return render(request, 'sales/index.html', context)


def generating_report(request):
    '''
    Actual function to show the report
    '''
    # Loading the data from sale
    data = Sale.objects.all()

    # Getting the minimum value of year in the database
    min_year = Sale.objects.all().aggregate(minimum=Min('year'))
    start_year = int(min_year['minimum'])

    # Getting the maximum value of year in the database
    max_year = Sale.objects.all().aggregate(maximum=Max('year'))
    end_year = int(max_year['maximum'])

    # Looping through the petroleum products and storing it in a list
    list_for_petroleum = []
    for i in data:
        if i.petroleum_product not in list_for_petroleum:
            list_for_petroleum.append(i.petroleum_product)

    # creating an empty dictionary
    empty_dictionary = {}

    # looping throught the list of petroleum products and calculating Avg, Min and Max
    for i in list_for_petroleum:
        # x is a value to make sure intervals of the petroleum in unique in dictionary
        x = 1
        # loop throught the data with provided range of years and specific filters
        while end_year > start_year:
            data = Sale.objects.filter(
                year__range=[end_year-4, end_year], petroleum_product=i).exclude(sale=0)

        # add the petroleum product to the dictionary in the interval
            empty_dictionary[i + str(x)] = [f"{end_year-4}-{end_year}", data.aggregate(
                Min('sale')), data.aggregate(Max('sale')), data.aggregate(Avg('sale'))]
            end_year -= 5
        # increment x by 1 everytime so that the data does not overlap in dictionary
            x += 1
        # change the end year back to the maximum year in the database
        end_year = int(max_year['maximum'])

    context = {
        'data': empty_dictionary,
    }

    return render(request, 'sales/report.html', context)
