# Young Innovations Internship Challenge

A challenge for internship at younginnovations

Link to the Solution -> [Click Me](https://yiplinternship.herokuapp.com/)

You can visit the final result from the link above or manually copying the link provided https://yiplinternship.herokuapp.com/

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install the required packages.

```
cd to the directory where requirements.txt is located
activate your virtualenv
run: pip install requirements.txt
open the root folder of manage.py in terminal
run python manage.py runserver
open http://127.0.0.1:8000/ in your browser
```
